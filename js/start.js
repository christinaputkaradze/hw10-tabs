
let titles = document.querySelectorAll('.tabs-title');

for (let i = 0; i < titles.length; i++) {
    titles[i].addEventListener('click', (event) => {

        let oldActive = document.querySelector('.tabs-title.active');
        oldActive.classList.remove('active');

        event.target.classList.add('active');

        let activeContent = document.querySelector('[data-active-content]');
        activeContent.removeAttribute('data-active-content');

        let contents = document.querySelectorAll('.tabs-content > li');
        let content = contents[i];
        content.setAttribute('data-active-content', '');
    });
};







